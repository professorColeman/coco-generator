# COCO Generator Example
![](screenshot.jpg)
This example shows how to generate a basic graphic classifier image and send it to RunwayML to be turned into a "real image." 
It uses the official ofxRunway addon and openFrameworks.