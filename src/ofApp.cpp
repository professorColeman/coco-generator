#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    //start the runway instance
    runway.setup(this, "http://localhost:8000");
    runway.start();
    //slow it down to 1fps
    ofSetFrameRate(1);
    
    //allocate our outgoing and returned images
    send2Runway.allocate(640, 360, OF_IMAGE_COLOR);
    getRunway.allocate(640, 360, OF_IMAGE_COLOR);
}

//--------------------------------------------------------------
void ofApp::update(){
    
    //send our image to runway
    if(!runway.isBusy()){
        ofxRunwayData data;
        data.setImage("semantic_map", send2Runway, OFX_RUNWAY_JPG);
        runway.send(data);
        var++; //keep track of how many images
    }
    //get our reculting image back
    runway.get("output", getRunway);
}

//--------------------------------------------------------------
void ofApp::draw(){
    //the backdrop color
    ofSetHexColor(0x5fdbff);
    ofDrawRectangle(0, 0, 640, 360);
    //the large circle color
    ofSetHexColor(0x3c3732);
    ofDrawCircle(320, 180, 100);
    //the small rectangles color
    ofSetHexColor(0x3f806e);

    float time = ofGetFrameNum();
    for(int i = 0; i<10; i++){ //animate the blocks
        float move = sin(i*time/10.0)*60.0;
        ofDrawRectangle(i*60, 180+move, 45, 20);
    }
    //grab the image to send
    send2Runway.grabScreen(0, 0, 640, 360);
    
    ofSetColor(255); //set color to white so resulting image is the right color
    if(getRunway.isAllocated()){getRunway.draw(640,0);}
    getRunway.save("zebraHerd" + to_string(var) +".jpg"); //save our frames
}

//--------------------------------------------------------------
// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info){
    ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message){
    ofLogNotice("ofApp::runwayErrorEvent") << message;
}
//--------------------------------------------------------------
