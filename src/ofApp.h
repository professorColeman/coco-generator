#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener{

	public:
		void setup();
		void update();
		void draw();

    ofxRunway runway;
    ofImage send2Runway, getRunway;
    int var;
    
    // Callback functions that process what Runway sends back
    void runwayInfoEvent(ofJson& info);
    void runwayErrorEvent(string& message);
		
};
